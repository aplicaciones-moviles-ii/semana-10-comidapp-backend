const db = require("../models");
const config = require("../config/auth_config");
const User = db.user;
const Role = db.role;
const Food = db.food;
const Event = db.events;
const Reservation = db.reservations;
const foodImage = db.foodImage;
const Op = db.Sequelize.Op;
const Sequelize = require('sequelize')

exports.createFood = async (req, res) => {
  try {
    const {
      name,
      description,
      pvp,
      latitude,
      longitude,
      city,
      max_tourist,
      image,
      images,
    } = req.body;
    const result = await Food.create({
      name,
      description,
      pvp,
      latitude,
      longitude,
      city,
      max_tourist,
      image,
      userId: req.userId,
    });
    const { id } = result.dataValues;
    if (images) {
      for await (const { url } of images) {
        await foodImage.create({ url, foodId: id });
      }
    }
    return res
      .status(201)
      .send({
        status: true,
        message: "Comida registrada correctamente!",
        foodId: id,
      });
  } catch (error) {
    console.log(error);
    res.status(500).send({ status: false, message: "¡Ha ocurrido un error!" });
  }
};

exports.updateFood = async (req, res) => {
  try {
    const { id } = req.params;
    const food = await Food.findOne({ where: { userId: req.userId, id } });
    if (!food) return res.status(422).json({ message: "Comida no encontrada" });
    const reservations = await Reservation.findAll({
      include: [
        {
          model: Event,
          where: { foodId: id },
        },
      ],
    });
    if (reservations.length) {
      return res
        .status(422)
        .json({
          message: "No se puede modificar comida que tiene reservaciones",
        });
    }
    const {
      name,
      description,
      pvp,
      latitude,
      longitude,
      city,
      max_tourist,
      image,
      images,
    } = req.body;
    await Food.update(
      {
        name,
        description,
        pvp,
        latitude,
        longitude,
        city,
        max_tourist,
        image,
      },
      { where: { userId: req.userId, id } }
    );

    await foodImage.destroy({ where: { foodId :id } })
    if (images) {
      for await (const { url } of images) {
        await foodImage.create({ url, foodId: id });
      }
    }
    return res.status(201).send({
      status: true,
      message: "Comida modificada correctamente!",
      foodId: id,
    });
  } catch (error) {
    console.log(error);
    res.status(500).send({ status: false, message: "¡Ha ocurrido un error!" });
  }
};

exports.deleteFood = async (req, res) => {
  try {
    const { id } = req.params;
    const food = await Food.findOne({ where: { userId: req.userId, id } });
    if (!food) return res.status(422).json({ message: "Comida no encontrada" });
    const reservations = await Reservation.findAll({
      include: [
        {
          model: Event,
          where: { foodId: id },
        },
      ],
    });
    if (reservations.length) {
      return res
        .status(422)
        .json({
          message: "No se puede eliminar una comida que tiene reservaciones",
        });
    }
    await foodImage.destroy({ where: { foodId :id } });
    await Event.destroy({ where: { foodId: id } });
    await Food.destroy({ where: { userId: req.userId, id } });
    
    return res.status(201).send({
      status: true,
      message: "Comida eliminada correctamente!",
      foodId: id,
    });
  } catch (error) {
    console.log(error);
    res.status(500).send({ status: false, message: "¡Ha ocurrido un error!" });
  }
};


exports.getMyFoods = async (req, res) => {
  try {
    const foods = await Food.findAll({ where: { userId: req.userId } });
    return res.status(200).send({ status: true, foods: foods });
  } catch (error) {
    console.log(error.message);
    res.status(500).send({ status: false, message: "¡Ha ocurrido un error!" });
  }
};

exports.getMyFood = async (req, res) => {
  try {
    const { id } = req.params;
    const food = await Food.findOne({ where: { userId: req.userId, id } });
    if (food) {
      return res.status(200).send({ status: true, food: food });
    } else {
      return res
        .status(200)
        .send({ status: true, message: "No existe esa comida" });
    }
  } catch (error) {
    console.log(error.message);
    res.status(500).send({ status: false, message: "¡Ha ocurrido un error!" });
  }
};

exports.getFoodByCity = async (req, res) => {
  try {
    const { city } = req.params;
    const foods = await Food.findAll({ where: { city } });
    if (foods) {
      return res.status(200).send({ status: true, foods });
    } else {
      return res
        .status(200)
        .send({ status: true, message: "No se encontraron resultados" });
    }
  } catch (error) {
    console.log(error.message);
    res.status(500).send({ status: false, message: "¡Ha ocurrido un error!" });
  }
};

exports.getFoodsByUser = async (req, res) => {
  try {
    const { userId } = req.params;
    const foods = await Food.findAll({ where: { userId } });
    if (foods) {
      return res.status(200).send({ status: true, foods });
    } else {
      return res
        .status(200)
        .send({ status: true, message: "No se encontraron resultados" });
    }
  } catch (error) {
    console.log(error.message);
    res.status(500).send({ status: false, message: "¡Ha ocurrido un error!" });
  }
};

exports.getReservationsByChef = async (req, res) => {
  try {
    const { userId } = req.params;
    const reservas = await db.sequelize.query(`
    select r.*, f.id "foodId"
    from events e 
    join foods f on f.id = e."foodId"
    join reservations r on r."eventId" = e.id
    join users u on u.id = r."userId"
    where f."userId" = ${userId}
    `)
    if (reservas[0].length > 0) {
      return res.status(200).send({ status: true, reservas: reservas[0] });
    } else {
      return res.status(200).send({ status: true, reservas: [] });
    }
  } catch (error) {
    console.log(error.message);
    res.status(500).send({ status: false, message: "¡Ha ocurrido un error!" });
  }
};

exports.getFoodDetail = async (req, res) => {
  try {
    const { id } = req.params;
    const food = await Food.findOne({ where: { id } });
    if (food) {
      return res.status(200).send({ status: true, food: food });
    } else {
      return res
        .status(200)
        .send({ status: true, message: "No existe esa comida" });
    }
  } catch (error) {
    console.log(error.message);
    res.status(500).send({ status: false, message: "¡Ha ocurrido un error!" });
  }
};
