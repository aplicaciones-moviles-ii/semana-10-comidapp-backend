const db = require("../models");
const User = db.user;
const Op = db.Sequelize.Op;

exports.getUser = async (req, res) => {
  try {
    const { id } = req.params;
    const user = await User.findOne({ where: { id: id } });
    if (user){
      return res.status(200).send({ status: true, user: user });
    }else{
      return res.status(200).send({ status: true, message: 'No existe ningun usuario con ese ID' });
    }
  } catch (error) {
    console.log(error.message);
    res.status(500).send({ status: false, message: "¡Ha ocurrido un error!" });
  }
};

exports.updateUser = async (req, res) => {
  try {
    const { id } = req.params;
    const user = await User.findOne({ where: { id: id } });
    if (!user) return res.status(422).json({ message: "Usuario no encontrado" });
    
    const {
      name, 
      last_name, 
      about_me, 
      latitude, 
      longitude, 
      photo, 
      direction, 
      telephone
    } = req.body;
    await User.update(
      {
        name, 
        last_name, 
        about_me, 
        latitude, 
        longitude, 
        photo, 
        direction, 
        telephone
      },
      { where: { id: id } }
    );

    return res.status(201).send({
      status: true,
      message: "Usuario modificado correctamente!"
    });

  } catch (error) {
    console.log(error);
    res.status(500).send({ status: false, message: "¡Ha ocurrido un error!" });
  }
};