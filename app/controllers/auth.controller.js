const db = require("../models");
const config = require("../config/auth_config");
const User = db.user;
const Op = db.Sequelize.Op;
const jwt = require("jsonwebtoken");
const bcrypt = require("bcryptjs");

exports.signup = async (req, res) => {
  try {
    const {
      email,
      password,
      name,
      last_name,
      about_me,
      photo,
      direction,
      telephone,
    } = req.body;
    await User.create({
      email,
      password: bcrypt.hashSync(password, 8),
      name,
      last_name,
      about_me,
      photo,
      direction,
      telephone,
    })
    return res.status(201).send({ status: true, message: "¡Usuario registrado correctamente!" });
  } catch (error) {
    console.log(error)
    return res.status(500).send({ status: false, message: 'Ha ocurrido un error' });
  }
};

exports.signin = (req, res) => {
  try {
    const user = req.user; // user found
    const passwordIsValid = bcrypt.compareSync(req.body.password, user.password);
    if (!passwordIsValid) {
      return res
        .status(401)
        .send({ accessToken: null, message: "Contraseña incorrecta" });
    }
    const { id, name, last_name, email, photo } = user;
    const token = jwt.sign({ id }, config.secret, { expiresIn: 86400 });
    return res.status(200).send({
      id,
      name,
      last_name,
      email,
      photo,
      accessToken: token,
    });
  } catch (error) {
    console.log(error)
    return res.status(500).send({ status: false, message: 'Ha ocurrido un error' });
  }
 
};
