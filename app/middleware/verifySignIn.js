const { body, validationResult } = require('express-validator');
const db = require("../models");
const User = db.user;

// main validation middleware
validateSignIn = (req, res, next) => {
  const errors = validationResult(req);
  if (!errors.isEmpty())
    return res.status(422).json({ status: false, message: errors.array()[0].msg });
  return next();
};

// rules validation
signInRules = () => {
  return [
    body("email")
      .trim()
      .notEmpty()
      .withMessage("Correo electrónico requerido")
      .isEmail()
      .withMessage("Correo electrónico no válido")
      .isLength({ max: 50 })
      .withMessage("El campo email no puede superar los 50 caracteres")
      .custom((email, { req }) => checkEmail(req, email)),
    body("password")
      .trim()
      .notEmpty()
      .withMessage("Contraseña requerida")
      .isLength({ min:5, max: 50 })
      .withMessage("La contraseña debe tener entre 8 y 25 caracteres")
  ];
};


const checkEmail = async (req, email) => {
  const user = await  User.findOne({ where: { email } })
  if (!user) return Promise.reject(`No existe un usuario registrado con este correo electrónico`);
  else req.user = user; // sends user data to the request
};


const verifySignIn = {
  validateSignIn,
  signInRules
};

module.exports = verifySignIn;
