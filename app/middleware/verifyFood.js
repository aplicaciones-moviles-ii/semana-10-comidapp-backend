const { body, validationResult } = require('express-validator');
const db = require("../models");
const Food = db.food;

// main validation middleware
validateFood = (req, res, next) => {
  const errors = validationResult(req);
  if (!errors.isEmpty())
    return res.status(422).json({ message: errors.array()[0].msg });
  return next();
};

// rules validation
foodRules = () => {
  return [
    body("name")
      .trim()
      .notEmpty()
      .withMessage("Nombre requerido")
      .isLength({ max: 50 })
      .withMessage("El campo nombre no puede superar los 50 caracteres"),
    body("description")
      .trim()
      .notEmpty()
      .withMessage("Descripción requerida")
      .isLength({ max: 200 })
      .withMessage("El campo descripción no puede superar los 200 caracteres"),
    body("pvp")
      .trim()
      .notEmpty()
      .withMessage("Precio requerido")
      .isDecimal()
      .withMessage("El campo precio debe tener un valor decimal"),
    body("latitude")
      .trim()
      .notEmpty()
      .withMessage("Latitud requerida")
      .isFloat({ min: -90, max: 90 })
      .withMessage("Latitud inválida"),
    body("longitude")
      .trim()
      .notEmpty()
      .withMessage("Longitud requerida")
      .isFloat({ min: -180, max: 180 })
      .withMessage("Longitud inválida")
  ];
};

const verifyFood = {
  validateFood,
  foodRules
};

module.exports = verifyFood;
