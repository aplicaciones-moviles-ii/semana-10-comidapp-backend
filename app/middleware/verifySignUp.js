const { body, validationResult } = require('express-validator');
const db = require("../models");
const ROLES = db.ROLES;
const User = db.user;

// main validation middleware
validateSignUp = (req, res, next) => {
  const errors = validationResult(req);
  if (!errors.isEmpty())
    return res.status(422).json({ status: false, message: errors.array()[0].msg });
  return next();
};

// rules validation
signUpRules = () => {
  return [
    body("email")
      .trim()
      .notEmpty()
      .withMessage("Correo electrónico requerido")
      .isEmail()
      .withMessage("Correo electrónico no válido")
      .isLength({ max: 50 })
      .withMessage("El campo email no puede superar los 50 caracteres")
      .custom((email, { req }) => checkDuplicateEmail(req, email)),
    body("password")
      .trim()
      .notEmpty()
      .withMessage("Contraseña requerida")
      .isLength({ min:5, max: 50 })
      .withMessage("La contraseña debe tener entre 8 y 25 caracteres")
      .custom((password, { req }) => checkStrongPassword(req, password)),
    body("name")
      .trim()
      .notEmpty()
      .withMessage("Nombre requerido")
      .isLength({ max: 50 })
      .withMessage("El campo nombre no puede superar los 50 caracteres"),
    body("last_name")
      .trim()
      .notEmpty()
      .withMessage("Apellido requerido")
      .isLength({ max: 50 })
      .withMessage("El campo apellido no puede superar los 50 caracteres"),
    body("about_me")
      .trim()
      .notEmpty()
      .withMessage("Acerca de mí requerido")
      .isLength({ min: 10, max: 200 })
      .withMessage("El campo Acerca de mí debe tener entre 12 y 200 caracteres"),
    body("photo")
      .trim()
      .notEmpty()
      .withMessage("Imagen requerida"),
    body("direction")
      .trim()
      .notEmpty()
      .withMessage("Dirección requerida")
      .isLength({ min: 10, max: 60 })
      .withMessage("El campo dirección debe tener entre 10 y 60 caracteres"),
  ];
};


const checkDuplicateEmail = async (req, email) => {
  const user = await  User.findOne({ where: { email } })
  if (user) return Promise.reject(`Ya existe un usuario registrado con este correo electrónico`);
};

const checkStrongPassword = async (req, password) => {
  // add validation
  const strong = true
  if (!strong) return Promise.reject(`Contraseña demasiado débil`);
};


checkRolesExisted = (req, res, next) => {
  if (req.body.roles) {
    for (let i = 0; i < req.body.roles.length; i++) {
      if (!ROLES.includes(req.body.roles[i])) {
        res.status(400).send({
          message: "Failed! Role does not exist = " + req.body.roles[i],
        });
        return;
      }
    }
  }
  next();
};

const verifySignUp = {
  validateSignUp,
  signUpRules,
  checkDuplicateEmail,
  checkRolesExisted,
};

module.exports = verifySignUp;
