const { authJwt } = require("../middleware");
const foodController = require("../controllers/food.controller");
const verifyFood = require('../middleware/verifyFood')

module.exports = function(app) {
  app.use(function(req, res, next) {
    res.header(
      "Access-Control-Allow-Headers",
      "x-access-token, Origin, Content-Type, Accept"
    );
    next();
  });
  app.get('/api/myFoods', authJwt.verifyToken, foodController.getMyFoods)
  app.get('/api/myFoods/:id', authJwt.verifyToken, foodController.getMyFood)
  app.post(
    "/api/food",
    [ 
      authJwt.verifyToken,
      verifyFood.foodRules(),
      verifyFood.validateFood,
    ],
    foodController.createFood
  );

  app.get('/api/foodsByCity/:city', authJwt.verifyToken, foodController.getFoodByCity)
  app.get('/api/foodsByUser/:userId', authJwt.verifyToken, foodController.getFoodsByUser)
  app.get('/api/getReservationsByChef/:userId', authJwt.verifyToken, foodController.getReservationsByChef)

  app.get('/api/getFoodDetail/:id', authJwt.verifyToken, foodController.getFoodDetail)
  app.put('/api/updateFood/:id', authJwt.verifyToken, foodController.updateFood)
  app.delete('/api/deleteFood/:id', authJwt.verifyToken, foodController.deleteFood)


};