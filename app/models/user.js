module.exports = (sequelize, Sequelize) => {
    const User = sequelize.define("users", {
      email: {
        type: Sequelize.STRING
      },
      password: {
        type: Sequelize.STRING
      },
      telephone: {
        type: Sequelize.STRING
      },
      name: {
        type: Sequelize.STRING
      },
      last_name: {
        type: Sequelize.STRING
      },
      about_me: {
        type: Sequelize.STRING
      },
      photo: {
        type: Sequelize.STRING
      },
      direction: {
        type: Sequelize.STRING
      },
    });
    return User;
  };