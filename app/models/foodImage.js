module.exports = (sequelize, Sequelize) => {
  const foodImage = sequelize.define("foodImages", {
    url: {
      type: Sequelize.STRING,
    },
  });
  return foodImage;
};
