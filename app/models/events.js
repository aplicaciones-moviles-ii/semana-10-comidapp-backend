module.exports = (sequelize, Sequelize) => {
    const Events = sequelize.define("events", {
      estado: {
        type: Sequelize.BOOLEAN,
        defaultValue: true
      },
      date: {
        type: Sequelize.STRING
      },
      hour: {
        type: Sequelize.STRING
      },
    });
    return Events;
  };